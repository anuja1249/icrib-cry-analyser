# iCrib Cry analyser

Baby cry analysis based on https://github.com/gveres/donateacry-corpus and https://github.com/karoldvl/ESC-50 dataset.

The files:
    - Lyd_lab.ipynb: lab for data import, cleaning, training and export models
    - my_model.pkl: pickled random forest classifier
    - my_model_pca.pkl: pickled pca model
    - myfile.py: prediction script for deployment

What is not included and have to be downloaded from other sources:
    - Baby cry dataset
    - Reference sounds dataset

Baby cry dataset:
    Download the audiofiles from https://github.com/gveres/donateacry-corpus and 
    convert it to wave

Reference sound:
    Download from https://github.com/karoldvl/ESC-50. There are some baby crying 
    sounds in this set and they have to be removed.

License:
    This software is licensed under GNU general license
    https://gitlab.com/icrib-ariot2019/icrib-cry-analyser/blob/master/LICENSE

    